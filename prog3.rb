#Define a method sum_to_n?
# which takes an array of integers and an additional integer, n, as arguments
# and returns true if any two distinct elements in the array of integers sum to n.
# An empty array or single element array should both return false.
def sum_to_n?(array_of_numbers,n)
	if array_of_numbers.empty? || array_of_numbers.length==1
		return false
	else
		for i in 0...array_of_numbers.length do
			for j in i+1...array_of_numbers.length do
				if (array_of_numbers[i]+array_of_numbers[j])== n
					return true
				end			

			end

		end
	end
	return false
end
puts sum_to_n([1,2,3,4,5],)
puts sum_to_n([3],3)
puts sum_to_n([],3)
puts sum_to_n([3,5,2,7,9],26)
