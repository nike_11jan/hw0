#Define a method max_2_sum 
#which takes an array of integers as an argument 
#and returns the sum of its two largest elements.
# For an empty array it should return zero.
# For an array with just one element, it should return that element.
def max_2_sum(arr_no)
	if arr_no.length == 0 
		return 0
	elsif arr_no.length == 1 
		return arr_no[0]
	elsif arr_no.length == 2
		return (arr_no[0] + arr_no[1])
	else
		arr=0
		arr = arr_no.sort.reverse
		return (arr[0] + arr[1])
	end
end

puts max_2_sum([1,5,8,3,9])
puts max_2_sum([1,5])
puts max_2_sum([2])
puts max_2_sum([])

