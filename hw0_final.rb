#Define a method sum which takes an array of integers as an argument 
#and returns the sum of its elements. 
#For an empty array it should return zero.
#function is to add array of numbers.
#It return 0 incase array is blank
def sum(arr_no)
	result=0
	if arr_no.length == 0 then
		return result
	else 
		arr_no.each {|a| result=result+a}
		return result
	end
end

#Define a method max_2_sum 
#which takes an array of integers as an argument 
#and returns the sum of its two largest elements.
# For an empty array it should return zero.
# For an array with just one element, it should return that element.
def max_2_sum(arr_no)
	if arr_no.length == 0 
		return 0
	elsif arr_no.length == 1 
		return arr_no[0]
	elsif arr_no.length == 2
		return (arr_no[0] + arr_no[1])
	else
		arr=0
		arr = arr_no.sort.reverse
		return (arr[0] + arr[1])
	end
end


#Define a method sum_to_n?
# which takes an array of integers and an additional integer, n, as arguments
# and returns true if any two distinct elements in the array of integers sum to n.
# An empty array or single element array should both return false.
def sum_to_n?(array_of_numbers,n)
	if array_of_numbers.empty? || array_of_numbers.length==1
		return false
	else
		for i in 0...array_of_numbers.length do
			for j in i+1...array_of_numbers.length do
				if (array_of_numbers[i]+array_of_numbers[j])== n
					return true
				end			

			end

		end
	end
	return false
end
=begin
result =sum([3,4,5,7])
puts "with arg :#{result} without :#{sum([])}"	

puts max_2_sum([1,5,8,3,9])
puts max_2_sum([1,5])
puts max_2_sum([2])
puts max_2_sum([])


puts sum_to_n([3,5,2,7,9],16)
puts sum_to_n([3],3)
puts sum_to_n([],3)
puts sum_to_n([3,5,2,7,9],26)
=end
