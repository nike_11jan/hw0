#Define a method sum which takes an array of integers as an argument 
#and returns the sum of its elements. 
#For an empty array it should return zero.
#function is to add array of numbers.
#It return 0 incase array is blank
def sum(arr_no)
	result=0
	if arr_no.length == 0 then
		return result
	else 
		arr_no.each {|a| result=result+a}
		return result
	end
end
result =sum([3,4,5,7])
puts "with arg :#{result} without :#{sum([])}"	

